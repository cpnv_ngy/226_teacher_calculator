﻿using System;
using Model;

namespace Calculator
{
    /// <summary>
    /// This class is designed to be a Calculator
    /// Ref : https://www.w3schools.com/cs/cs_user_input.asp
    /// Ref : https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/static
    /// Ref : https://docs.microsoft.com/en-us/dotnet/api/system.console.writeline?view=netcore-3.1
    /// Ref : https://docs.microsoft.com/en-us/dotnet/api/system.console.readline?view=netcore-3.1
    /// Ref : https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/switch
    /// </summary>
    class Program
    {
        #region Variables declaration
        static private int result;
        static private int op1;
        static private int op2;
        static private char ope;
        static private Maths maths;
        #endregion Variables declaration

        /// <summary>
        /// This method is the entry point of the application
        /// </summary>
        /// <param name="args"></param>
        static private void Main(string[] args)
        {

            maths = new Maths();

            do
            {
                AskForUserInput();

                result = ManageOperator(ope);

                Console.WriteLine("The result of " + op1 + ope + op2 + " is : " + result);
        
            }while (ope != '#') ;
        }

        #region User input and variable initialization
        /// <summary>
        /// This method asks the user to input both operandes and operator
        /// </summary>
        static private void AskForUserInput()
        {
            Console.Write("**********************\r\n");

            Console.Write("Enter first operand : ");
            op1 = int.Parse(Console.ReadLine());
            
            Console.Write("Enter operator (+, -, *, /) : ");
            ope = char.Parse(Console.ReadLine());

            Console.Write("Enter second operand : ");
            op2 = int.Parse(Console.ReadLine());
        }
        #endregion User input and variable initialization

        #region Perform calculations
        /// <summary>
        /// This method is defined to select the right operation according to the operator detected (user input)
        /// </summary>
        /// <param name="ope"></param>
        /// <returns></returns>
        static private int ManageOperator(char ope)
        {
            switch (ope)
            {
                case '+':
                    result = maths.Add(op1, op2);
                    break;
                case '-':
                    result = maths.Substract(op1, op2);
                    break;
                case '*':
                    result = maths.Multiply(op1, op2);
                    break;
                case '/':
                    if(op2 != 0)
                    {
                        result = maths.Divide(op1, op2);
                    }
                    else
                    {
                        Console.WriteLine("Divide by zero is not possible ;(");
                    }
                    
                    break;
                default:
                    Console.WriteLine("Operator not supported ;(");
                    result = 0;
                    break;
            }
            return result;
        }
        #endregion Perform calculations
}
}
