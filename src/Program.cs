﻿using System;

namespace Calculator
{
    /// <summary>
    /// <description>This class is designed to be a Calculator</description>
    /// <remarks>"https://www.w3schools.com/cs/cs_user_input.asp"</remarks>
    /// Ref : https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/static
    /// Ref : https://docs.microsoft.com/en-us/dotnet/api/system.console.writeline?view=netcore-3.1
    /// Ref : https://docs.microsoft.com/en-us/dotnet/api/system.console.readline?view=netcore-3.1
    /// <remarks>My remark</remarks>
    /// </summary>
    class Program
    {
        /// <summary>
        /// This method is the entry point of the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            #region Variables declaration
            int result;
            int op1;
            int op2;
            #endregion Variables declaration


            #region User input and variable initialization
            Console.Write("Enter first operand : ");
            op1 = int.Parse(Console.ReadLine());

            Console.Write("Enter second operand : ");
            op2 = int.Parse(Console.ReadLine());
            #endregion User input and variable initialization


            #region Perform the calculations
            result = Add(op1, op2);
            #endregion Perform the calculations


            #region Display result
            Console.WriteLine("The sum of " + op1 + " and " + op2 + " is : " + result);
            #endregion Display result
        }

        /// <summary>
        /// This method is designed to add two integers
        /// </summary>
        /// <param name="op1">First operand</param>
        /// <param name="op2">Second operand</param>
        /// <returns>The result, in integer</returns>
        static int Add(int op1, int op2)
        {
            return op1 + op2;
        }
    }
}
